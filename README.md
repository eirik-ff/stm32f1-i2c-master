# I2C Master breakout board

Provides a simple USB interface to operate as master on an I2C bus. Also has capabilities to control a LED matrix screen. 


## USB interface
The USB interface is a text based interface with simple commands as the following help message outlines. It can be controlled through a serial terminal program such as [RealTerm](https://sourceforge.net/projects/realterm/). 

```
Usage (all numbers must be written in hex)                                        
 ?             Help message                                                 
 H             whoami from HTS211                                              
 Estr          Echo string str        
 Txxyyd0..dN   Transmit yy bytes to address xx (no space between bytes)
 Wxxyyzzd0..dN Write zz bytes to register yy at address xx
 Rxxyyzz       Read zz bytes from register yy at address xx
 Dxxzz         Receive zz bytes from address xx without setting register
 Sb[tt][h][q]  Start (b=1) or stop (b=0) streaming with at interval of 
               tt (in hex) ms (valid range: 5-250, default is 10 ms). If
               q is present, enter quiet mode (no print to USB) (default off)
               If h is present, stream data as hex
```

Do not put spaces between arguments. 

### Examples

Receive 12 (0x0C) bytes from register 0x20 from slave with address 0x42: `R42200C`

Read 4 bytes from where the register pointer is currently pointing in slave with address 0x42: `D4204`

Write 4 bytes (0xDE, 0xAD, 0xBE, 0xEF) to register 0x10 on slave with address 0x42: `W421004DEADBEEF`

Start streaming with 100 ms (0x64) delay between each sample: `S164`

Stop streaming: `S0`


### Streaming
This mode will repeatedly, with the given interval, read the sensor data from the I2C bus and print it over USB. The data is printed out tab-separated and new-line terminated, and in the following order:

```
RPM    PCV    Pressure    Baseline  Diff    Touch     Force estimate      Aperture estimate    Force level    Aperture level \n
```

## I2C pinout

| SCL  | SDA  |
|------|------|
| PB10 | PB11 |

