#ifndef LED_H_INCLUDED
#define LED_H_INCLUDED

#include "stm32f10x_conf.h"

#define LED_PIN GPIO_Pin_13
#define LED_PORT GPIOC
#define LED_ON() (LED_PORT->BSRR = LED_PIN<<16)
#define LED_OFF() (LED_PORT->BSRR = LED_PIN)


void led_init(void);


#endif /* LED_H_INCLUDED */
