/*
**
**                           Main.c
**
**
**********************************************************************/
/*
   Last committed:     $Revision: 00 $
   Last changed by:    $Author: $
   Last changed date:  $Date:  $
   ID:                 $Id:  $

**********************************************************************/
#include "stm32f10x_conf.h"
#include <stdio.h>
#include <string.h>
#include "usblib.h"
#include <stdatomic.h>
#include <stdbool.h>
#include "i2c2.h"
#include "led.h"
#include "util.h"
#include "fifo.h"


#define MYHAND_I2C_SLAVE_ADDR 0x42
#define MYHAND_I2C_WORD_SIZE 4


RCC_ClocksTypeDef clocks;
volatile uint32_t sTick = 0;

USBLIB_WByte LineState;
volatile bool lineChanged = false;

bool txNow;
char usbTxBuf[256];

bool stream_data = false;
bool stream_hex = false;
bool stream_quiet = false;
volatile uint32_t count_since_last_transfer = 0;
uint32_t stream_interval = 100; // ms
union Sensor {
    uint32_t u32;
    int32_t i32;
    float f;
};


char usage[] = {"Usage (all numbers must be written in hex)\n"
                " ?             Help message\n"
                " H             whoami from HTS211\n"
                " Estr          Echo string str\n"
                " Txxyyd0..dN   Transmit yy bytes to address xx (no space between bytes)\n"
                " Wxxyyzzd0..dN Write zz bytes to register yy at address xx\n"
                " Rxxyyzz       Read zz bytes from register yy at address xx\n"
                " Dxxzz         Receive zz bytes from address xx without setting register\n"
                " Sb[tt][h][q]  Start (b=1) or stop (b=0) streaming with at interval of \n"
                "               tt (in hex) ms (valid range: 10-250, default is 100 ms). If\n"
                "               q is present, enter quiet mode (no print to USB) (default off)\n"
                "               If h is present, stream data as hex\n"};


void SysTick_Handler(void) {
    sTick++;
    count_since_last_transfer++;
}

// Using GPIO PA11 and PA12 for D- and D+ and PA15 for enabling D+ 1.5k (3.3v) pullup.
void initUSB(void) {
    GPIO_InitTypeDef gpioInit;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);

    GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE);

    GPIO_StructInit(&gpioInit);
    gpioInit.GPIO_Pin = GPIO_Pin_11 | GPIO_Pin_12;
    gpioInit.GPIO_Speed = GPIO_Speed_50MHz;
    gpioInit.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init(GPIOB, &gpioInit);

    gpioInit.GPIO_Pin = GPIO_Pin_15;
    gpioInit.GPIO_Speed = GPIO_Speed_2MHz;
    gpioInit.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(GPIOB, &gpioInit);

}

void uUSBLIB_LineStateHandler(USBLIB_WByte state) {
    LineState = state;
    lineChanged = true;
}



void whoami_test(void) {
    // whoami test for HTS221
    const uint8_t hts221_addr = 0xbe;
    uint8_t whoami;
    i2c2_receiveBytesFromReg(hts221_addr, 0x0f, &whoami, 1);
    if (whoami == 0xBC) {
        sprintf(usbTxBuf, "Got correct whoami: 0x%.2X\n", whoami);
    }
    else {
        sprintf(usbTxBuf, "Got wrong whoami: 0x%.2X\n", whoami);
    }
    txNow = true;
}

void echo_string(void) {
    // echo string
    uint8_t buffer[FIFO_SIZE];
    uint8_t *p = buffer;
    while (fifoDataCount) {
        *p++ = fifoGet();
    }
    *p = 0;
    sprintf(usbTxBuf, "%s", buffer);
    txNow = true;
}

void transmit_bytes(void) {
    // Transmit bytes
    // Syntax: Txxyyd0d1..dN
    // xx: address in hex
    // yy: number of bytes
    // di: byte_i
    uint8_t buffer[255] = {0};
    uint8_t addr = getHexByteFromFifo();
    uint8_t byteCount = getHexByteFromFifo();
    for (int i = 0; i < byteCount; i++) {
        buffer[i] = getHexByteFromFifo();
    }

    int status = i2c2_transmitBytes(addr, buffer, byteCount);
    sprintf(usbTxBuf, "(%s) Transmit %d bytes to addr 0x%.2X: ",
            status == 0 ? "success":"error", byteCount, addr);
    for (int i = 0; i < byteCount; i++) {
        char byteInHex[4] = {0};
        sprintf(byteInHex, "%.2X ", buffer[i]);
        strncat(usbTxBuf, byteInHex, 3);
    }
    strcat(usbTxBuf, "\n");
    txNow = true;
}

void write_to_register(void) {
    // Write to register
    // Syntax: Wxxyyzzd0..dN
    // xx: address in hex
    // yy: register in hex
    // zz: how many bytes (in hex) to write
    // di: byte_i to write
    uint8_t buffer[255] = {0};
    // for loop is needed to give time for the fifo buffer to fill up
    for (int i = 0; i < 10000; i++);
    uint8_t addr = getHexByteFromFifo();
    uint8_t reg = getHexByteFromFifo();
    buffer[0] = reg;
    uint8_t byteCount = getHexByteFromFifo();
    for (int i = 1; i < byteCount + 1; i++) {
        buffer[i] = getHexByteFromFifo();
    }

    int status = i2c2_transmitBytes(addr, buffer, byteCount + 1);
    sprintf(usbTxBuf, "(%s) Write %d bytes to reg 0x%.2X to addr 0x%.2X: ",
            status == 0 ? "success":"error", byteCount, reg, addr);
    for (int i = 1; i < byteCount + 1; i++) {
        char byteInHex[4] = {0};
        sprintf(byteInHex, "%.2X ", buffer[i]);
        strncat(usbTxBuf, byteInHex, 3);
    }
    strcat(usbTxBuf, "\n");
    txNow = true;
}

void receive_from_register(void) {
    // Receive from reg
    // Syntax: Rxxyyzz
    // xx: address in hex
    // yy: register in hex
    // zz: size of transmission in hex
    uint8_t buffer[255] = {0};
    uint8_t addr = getHexByteFromFifo();
    uint8_t reg = getHexByteFromFifo();
    uint8_t byteCount = getHexByteFromFifo();
    if (byteCount > 255) byteCount = 255;  // cap number of bytes possible to recv

    int status = i2c2_receiveBytesFromReg(addr, reg, buffer, byteCount);
    sprintf(usbTxBuf, "(%s) Recv 0x%.2X bytes from reg 0x%.2X from addr 0x%.2X: ",
            status == 0 ? "success":"error", byteCount, reg, addr);
    for (int i = 0; i < byteCount; i++) {
        char byteInHex[4] = {0};
        sprintf(byteInHex, "%.2X ", buffer[i]);
        strncat(usbTxBuf, byteInHex, 3);
    }
    strcat(usbTxBuf, "\n");
    txNow = true;
}

void receive_bytes(void) {
    // Receive only
    // Syntax: Dxxzz
    // xx: address in hex
    // zz: size of transmission in hex
    uint8_t buffer[255] = {0};
    uint8_t addr = getHexByteFromFifo();
    uint8_t byteCount = getHexByteFromFifo();
    if (byteCount > 255) byteCount = 255;  // cap number of bytes possible to recv

    int status = i2c2_receiveBytes(addr, buffer, byteCount);
    sprintf(usbTxBuf, "(%s) Recv 0x%.2X bytes from addr 0x%.2X: ",
            status == 0 ? "success":"error", byteCount, addr);
    for (int i = 0; i < byteCount; i++) {
        char byteInHex[4] = {0};
        sprintf(byteInHex, "%.2X ", buffer[i]);
        strncat(usbTxBuf, byteInHex, 3);
    }
    strcat(usbTxBuf, "\n");
    txNow = true;
}

void stream_data_cmd_interpret(void) {
    // Stream sensor data from hand
    // Will repeatedly query the hand over i2c and print over usb
    // Syntax: Sb[tt][h][q]
    // b: bool, 0 for stop streaming, 1 for start streaming
    // tt: delay between each query
    // h: stream data in hex
    // q: enable quiet mode if present
    stream_data = hexCharToInt((char)fifoGet());
    stream_quiet = false;
    stream_hex = false;
    while (fifoDataCount) {
        uint8_t c = hexCharToInt((char)fifoGet());
        if (c == 'q') {
            stream_quiet = true;
        }
        else if (c == 'h') {
            stream_hex = true;
        }
        else if (c >= 0 && c <= 0xff) {
            uint8_t desired_interval = c << 4;
            c = hexCharToInt((char)fifoGet());
            desired_interval |= c;

            if (desired_interval < 10) {
                desired_interval = 10;
            }
            else if (desired_interval > 250) {
                desired_interval = 250;
            }
            stream_interval = desired_interval;
        }
    }

    if (stream_data) {
        sprintf(usbTxBuf, "---- Start stream %s, interval = 0x%.2lX ms ----\n",
                stream_quiet ? "(quiet)":"", stream_interval);
    }
    else {
        sprintf(usbTxBuf, "---- End stream ----\n");
    }
    txNow = true;
}

void transmit_stream_data(void) {
    uint8_t sensor_buf[64];
    uint8_t hand_i2c_addr = 0x42;
    uint8_t reg = 0x20;
    uint8_t Size = 10*4;  // bytes
    int status = i2c2_receiveBytesFromReg(hand_i2c_addr, reg, sensor_buf, Size);
    if (!status) {
        union Sensor rpm          = { .u32 = bufTo32bitWordLittleEndian(&sensor_buf[0*4]) };
        union Sensor pcv          = { .u32 = bufTo32bitWordLittleEndian(&sensor_buf[1*4]) };
        union Sensor pressure     = { .u32 = bufTo32bitWordLittleEndian(&sensor_buf[2*4]) };
        union Sensor baseline     = { .u32 = bufTo32bitWordLittleEndian(&sensor_buf[3*4]) };
        union Sensor diff         = { .u32 = bufTo32bitWordLittleEndian(&sensor_buf[4*4]) };
        union Sensor touch        = { .u32 = bufTo32bitWordLittleEndian(&sensor_buf[5*4]) };
        union Sensor force_est    = { .u32 = bufTo32bitWordLittleEndian(&sensor_buf[6*4]) };
        union Sensor aperture_est = { .u32 = bufTo32bitWordLittleEndian(&sensor_buf[7*4]) };
        union Sensor force_lvl    = { .u32 = bufTo32bitWordLittleEndian(&sensor_buf[8*4]) };
        union Sensor aperture_lvl = { .u32 = bufTo32bitWordLittleEndian(&sensor_buf[9*4]) };

        if (!stream_quiet) {
            char usb_buf[128];
            if (stream_hex) {
                sprintf(usb_buf, "%lx\t%lx\t%lx\t%lx\t%lx\t%lx\t%lx\t%lx\t%lx\t%lx\n",
                    rpm.u32, pcv.u32, pressure.u32, baseline.u32, diff.i32, touch.u32, 
                    force_est.i32, aperture_est.i32, force_lvl.i32, aperture_lvl.u32);
            }
            else {
                sprintf(usb_buf, "%ld\t%lu\t%lu\t%lu\t%ld\t%ld\t%ld\t%ld\t%ld\t%ld\n",
                    rpm.u32, pcv.u32, pressure.u32, baseline.u32, diff.i32, touch.i32, 
                    force_est.i32, aperture_est.i32, force_lvl.i32, aperture_lvl.i32);
            }
            printToUSB(usb_buf, strlen(usb_buf));
        }
    }
}


int main(void) {
    led_init();
    initUSB(); // Init usb pins
    GPIOA->BSRR = (GPIO_Pin_15); // Reset pullup

    RCC_PCLK2Config(RCC_HCLK_Div1); // PCLK2 = 72 MHz
    RCC_USBCLKConfig(RCC_USBCLKSource_PLLCLK_1Div5); // 72/1.5=48 MHz

    RCC_PLLConfig(RCC_PLLSource_HSE_Div1,RCC_PLLMul_9); // 8/1*9=72 MHz
    RCC_PLLCmd(ENABLE);
    RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);

    RCC_GetClocksFreq(&clocks);

    SysTick_Config((clocks.HCLK_Frequency/8)/1000); // 1ms systick is nice
    SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK_Div8);

    i2c2_init(100000);

    for(int32_t n = 0; n < 3200000; n++);

    USBLIB_Init();
    GPIOA->BRR = (GPIO_Pin_15); // Set pullup


    while(1) {
        if (lineChanged) {
          lineChanged = false;
          if (LineState.L & 0x01) LED_ON();
          else LED_OFF();
        }

        if (fifoDataCount) {
            uint8_t byte = fifoGet();
            switch (byte) {
            case 'H':
                whoami_test();
                break;
            case 'E':
                echo_string();
                break;
            case 'T':
                transmit_bytes();
                break;
            case 'W':
                write_to_register();
                break;
            case 'R':
                receive_from_register();
                break;
            case 'D':
                receive_bytes();
                break;
            case 'S':
                stream_data_cmd_interpret();
                break;
            case '?':
                printToUSB(usage, strlen(usage));
                break;
            default:
                break;
            }
            while (fifoDataCount) fifoGet();  // exhaust buffer if extra chars left
        }

        if (txNow) {
            txNow = false;
            printToUSB(usbTxBuf, strlen(usbTxBuf));
            usbTxBuf[0] = 0;  // makes sprintf overwrite the previous data
        }

        if (stream_data && count_since_last_transfer >= stream_interval) {
            count_since_last_transfer = 0;
            transmit_stream_data();
        }
    }
}


