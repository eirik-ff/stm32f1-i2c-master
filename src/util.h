#ifndef UTIL_H_INCLUDED
#define UTIL_H_INCLUDED

#include "stm32f10x_conf.h"


#define DISABLE_INTERRUPTS()				__asm volatile 	( " cpsid i " )
#define ENABLE_INTERRUPTS()					__asm volatile 	( " cpsie i " )

#define MAP_RANGE(in, in_start, in_end, out_start, out_end) (out_start + ((out_end - out_start) / (in_end - in_start)) * (in - in_start))

uint8_t hexCharToInt(char c);
uint32_t bufTo32bitWordLittleEndian(uint8_t *buf);
void printToUSB(char *buffer, unsigned int length);


#endif /* UTIL_H_INCLUDED */
