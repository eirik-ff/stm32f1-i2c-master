#include "led.h"

// Using TIM2 for solenoid pwm for pins PA1 (TIM2_CH2) and PA2 (TIM2_CH3)
void led_init(void) {
    GPIO_InitTypeDef gpioInit;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);

    LED_OFF();

    gpioInit.GPIO_Pin = LED_PIN;
    gpioInit.GPIO_Speed = GPIO_Speed_2MHz;
    gpioInit.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(LED_PORT, &gpioInit);
}
