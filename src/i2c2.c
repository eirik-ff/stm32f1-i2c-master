#include "i2c2.h"
#include "stm32f10x_conf.h"

// private helper function in i2c2 init
static void delay(uint32_t t) {
  volatile int n;
  while(t) {
    for (n=3260;n;n--) {
      t--;
      t++;
    }
    t--;
  }
}

void i2c2_init(uint32_t clockrate) {
    I2C_InitTypeDef i2cInit;
    GPIO_InitTypeDef gpioInit;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C2, ENABLE);

    GPIO_StructInit(&gpioInit);
    gpioInit.GPIO_Pin = I2C2_SCL | I2C2_SDA;
    gpioInit.GPIO_Speed = GPIO_Speed_2MHz;
    gpioInit.GPIO_Mode = GPIO_Mode_AF_OD;
    GPIO_Init(GPIOB, &gpioInit);

    // Set Output bits on SCL (PB10) and SDA (PB11) to get some pullup possibilities
    GPIOB->BSRR = I2C2_SCL | I2C2_SDA;

    delay(100);

    I2C2->CR1 |= I2C_CR1_SWRST;
    I2C2->CR1 &= ~I2C_CR1_SWRST;

    delay(100);

    I2C_StructInit(&i2cInit);
    i2cInit.I2C_ClockSpeed = clockrate;
    i2cInit.I2C_Mode = I2C_Mode_I2C;
    I2C_Init(I2C2, &i2cInit);

    I2C_Cmd(I2C2, ENABLE);
}

int i2c2_transmitBytes(uint8_t addr, uint8_t *buffer, uint8_t bufSize) {
    uint32_t lastEvent;

    I2C_Cmd(I2C2, ENABLE);
    I2C_AcknowledgeConfig(I2C2,ENABLE); // Enable I2C acknowledgment

    I2C_GenerateSTART(I2C2,ENABLE);
    while (1) { // Wait for EV5
        lastEvent = I2C_GetLastEvent(I2C2);
        if (lastEvent & (I2C_SR1_ARLO | I2C_SR1_BERR))
            return 1; // Error!
        else if (lastEvent==I2C_EVENT_MASTER_MODE_SELECT)
            break;
    }

    I2C_Send7bitAddress(I2C2,addr,I2C_Direction_Transmitter); // Send slave address
    while (1) {
        lastEvent = I2C_GetLastEvent(I2C2);
        if ((lastEvent & I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED)==I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED)
            break; // Wait for EV6
        else if ((lastEvent == (I2C_SR1_AF | (I2C_SR2_BUSY | I2C_SR2_MSL)<<16)) || (lastEvent & (I2C_SR1_BERR | I2C_SR1_ARLO))) {
            I2C_GenerateSTOP(I2C2,ENABLE);

            while (I2C2->SR2 & I2C_SR2_BUSY);
            I2C_ClearFlag(I2C2, I2C_FLAG_AF | I2C_FLAG_BERR | I2C_FLAG_ARLO);

            return 1; // NACK of address mean abort! abort! abort!
        }
    }

    uint8_t byte;
    while (bufSize) {
        byte = *buffer++;
        bufSize--;

        I2C_SendData(I2C2,byte); // Send value
        while (!I2C_CheckEvent(I2C2,I2C_EVENT_MASTER_BYTE_TRANSMITTED)); // Wait for EV8
    }

    I2C_GenerateSTOP(I2C2,ENABLE);
    while (I2C2->SR2 & I2C_SR2_BUSY);

    I2C_Cmd(I2C2, DISABLE);

    return 0; // No error
}

int i2c2_receiveBytesFromReg(uint8_t addr, uint8_t reg, uint8_t *outBytes, uint8_t recvCount) {
    uint32_t lastEvent;
    I2C_Cmd(I2C2, ENABLE);

    I2C_AcknowledgeConfig(I2C2, ENABLE); // Enable I2C acknowledgment

    I2C_GenerateSTART(I2C2, ENABLE);
    while (1) { // Wait for EV5
        lastEvent = I2C_GetLastEvent(I2C2);
        if (lastEvent & (I2C_SR1_ARLO | I2C_SR1_BERR))
            return 1; // Error!
        else if (lastEvent==I2C_EVENT_MASTER_MODE_SELECT)
            break;
    }

    I2C_Send7bitAddress(I2C2,addr,I2C_Direction_Transmitter); // Send slave address with transmit direction
    while (1) {
        lastEvent = I2C_GetLastEvent(I2C2);
        if ((lastEvent & I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED)==I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED) {
            break; // Wait for EV6
        }
        else if ((lastEvent == (I2C_SR1_AF | (I2C_SR2_BUSY | I2C_SR2_MSL)<<16)) || (lastEvent & (I2C_SR1_BERR | I2C_SR1_ARLO))) {
            I2C_GenerateSTOP(I2C2,ENABLE);

            while (I2C2->SR2 & I2C_SR2_BUSY);
            I2C_ClearFlag(I2C2, I2C_FLAG_AF | I2C_FLAG_BERR | I2C_FLAG_ARLO);

            return 1; // NACK of address mean abort! abort! abort!
        }
    }

    I2C_SendData(I2C2, reg);
    while (!I2C_CheckEvent(I2C2,I2C_EVENT_MASTER_BYTE_TRANSMITTED)); // Wait for EV8

    if (recvCount == 0) {
        I2C_AcknowledgeConfig(I2C2, DISABLE);  // disable ack for last byte
        I2C_GenerateSTOP(I2C2,ENABLE);
    }
    else {
        I2C_GenerateSTART(I2C2, ENABLE);  // repeated start condition
        while (1) { // Wait for EV5
            lastEvent = I2C_GetLastEvent(I2C2);
            if (lastEvent & (I2C_SR1_ARLO | I2C_SR1_BERR))
                return 1; // Error!
            else if (lastEvent==I2C_EVENT_MASTER_MODE_SELECT)
                break;
        }

        I2C_Send7bitAddress(I2C2,addr,I2C_Direction_Receiver); // Send slave address with receive direction
        while (1) {
            lastEvent = I2C_GetLastEvent(I2C2);
            if ((lastEvent & I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED)==I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED)
                break; // Wait for EV6
            else if ((lastEvent == (I2C_SR1_AF | (I2C_SR2_BUSY | I2C_SR2_MSL)<<16)) || (lastEvent & (I2C_SR1_BERR | I2C_SR1_ARLO))) {
                I2C_GenerateSTOP(I2C2,ENABLE);

                while (I2C2->SR2 & I2C_SR2_BUSY);
                I2C_ClearFlag(I2C2, I2C_FLAG_AF | I2C_FLAG_BERR | I2C_FLAG_ARLO);

                return 1; // NACK of address mean abort! abort! abort!
            }
        }

        for (int i = 0; i < recvCount; i++) {
            if (i == recvCount - 1) {
                I2C_AcknowledgeConfig(I2C2, DISABLE);  // disable ack for last byte
                I2C_GenerateSTOP(I2C2,ENABLE);
            }
            while (!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_BYTE_RECEIVED));  // wait for EV7
            outBytes[i] = I2C_ReceiveData(I2C2);
        }
    }

    while (I2C2->SR2 & I2C_SR2_BUSY);
    I2C_GetLastEvent(I2C2);  // clears any uncleared flag, setting us up for success on the next transmission.

    I2C_Cmd(I2C2, DISABLE);
    return 0;  // no error
}

int i2c2_receiveBytes(uint8_t addr, uint8_t *outBytes, uint8_t recvCount) {
    uint32_t lastEvent;

    I2C_Cmd(I2C2, ENABLE);
    I2C_AcknowledgeConfig(I2C2, ENABLE); // Enable I2C acknowledgment

    if (I2C2->SR2 & I2C_SR2_BUSY) {
        I2C_Cmd(I2C2, DISABLE);
        return 1;
    }

    I2C_GenerateSTART(I2C2, ENABLE);  // start condition
    while (1) { // Wait for EV5
        lastEvent = I2C_GetLastEvent(I2C2);
        if (lastEvent & (I2C_SR1_ARLO | I2C_SR1_BERR))
            return 1; // Error!
        else if (lastEvent==I2C_EVENT_MASTER_MODE_SELECT)
            break;
    }

    I2C_Send7bitAddress(I2C2,addr,I2C_Direction_Receiver); // Send slave address with receive direction
    while (1) {
        lastEvent = I2C_GetLastEvent(I2C2);
        if ((lastEvent & I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED)==I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED)
            break; // Wait for EV6
        else if ((lastEvent == (I2C_SR1_AF | (I2C_SR2_BUSY | I2C_SR2_MSL)<<16)) || (lastEvent & (I2C_SR1_BERR | I2C_SR1_ARLO))) {
            I2C_GenerateSTOP(I2C2,ENABLE);

            while (I2C2->SR2 & I2C_SR2_BUSY);
            I2C_ClearFlag(I2C2, I2C_FLAG_AF | I2C_FLAG_BERR | I2C_FLAG_ARLO);

            return 1; // NACK of address mean abort! abort! abort!
        }
    }

    for (int i = 0; i < recvCount; i++) {
        if (i == recvCount - 1) {
            I2C_AcknowledgeConfig(I2C2, DISABLE);  // disable ack for last byte
            I2C_GenerateSTOP(I2C2,ENABLE);
        }
        while (!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_BYTE_RECEIVED));  // wait for EV7
        outBytes[i] = I2C_ReceiveData(I2C2);
    }

    while (I2C2->SR2 & I2C_SR2_BUSY);
    I2C_GetLastEvent(I2C2);  // clears any uncleared flag, setting us up for success on the next transmission.

    I2C_Cmd(I2C2, DISABLE);

    return 0;  // no error
}
