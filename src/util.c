#include "util.h"
#include "usblib.h"


uint8_t hexCharToInt(char c) {
    if (c >= '0' && c <= '9') {
        return (uint8_t)(c - '0');
    }
    else if (c >= 'a' && c <= 'f') {
        return 10 + (uint8_t)(c - 'a');
    }
    else if (c >= 'A' && c <= 'F') {
        return 10 + (uint8_t)(c - 'A');
    }
    return c;
}

uint32_t bufTo32bitWordLittleEndian(uint8_t *buf) {
    uint32_t ret = 0;
    for (int i = 0; i < 4; i++) {
        ret |= buf[i] << 8*i;
    }
    return ret;
}

void printToUSB(char *buffer, unsigned int length) {
    uint16_t tx_length = 64;
    int chars_remaining = length;
    while (chars_remaining) {
        if (chars_remaining < tx_length)
            tx_length = chars_remaining;
        USBLIB_Transmit(&buffer[length - chars_remaining], tx_length);
        chars_remaining -= tx_length;
    }
}
