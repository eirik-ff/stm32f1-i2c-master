#ifndef FIFO_H_INCLUDED
#define FIFO_H_INCLUDED

#include <stdint.h>
#include <stdbool.h>

#define FIFO_SIZE 256

extern volatile int fifoDataCount;

uint8_t fifoGet();
void fifoPut(uint8_t byte);
uint8_t getHexByteFromFifo();


#endif /* FIFO_H_INCLUDED */
