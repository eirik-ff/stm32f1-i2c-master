#ifndef I2C2_H
#define I2C2_H

#include <stdint.h>
#include "stm32f10x_conf.h"

#define I2C2_SCL GPIO_Pin_10
#define I2C2_SDA GPIO_Pin_11

void i2c2_init(uint32_t clockrate);
int i2c2_transmitBytes(uint8_t addr, uint8_t *buffer, uint8_t bufSize);
int i2c2_receiveBytesFromReg(uint8_t addr, uint8_t reg, uint8_t *outBytes, uint8_t recvCount);
int i2c2_receiveBytes(uint8_t addr, uint8_t *outBytes, uint8_t recvCount);

#endif // I2C2_H
