#include "fifo.h"
#include "util.h"
#include "usblib.h"


volatile uint8_t fifo[FIFO_SIZE];
uint8_t fifoReadIndex = 0;
uint8_t fifoWriteIndex = 0;
volatile int fifoDataCount = 0;

// Meant to be called from outside interrupt
uint8_t fifoGet() {
    uint8_t byte = fifo[fifoReadIndex++];
    if (fifoReadIndex >= FIFO_SIZE)
        fifoReadIndex = 0;
    DISABLE_INTERRUPTS();
    fifoDataCount--;
    ENABLE_INTERRUPTS();

    return byte;
}

// Meant to be called from interrupt
// Overflows are handled by simply not putting any data into fifo.
void fifoPut(uint8_t byte) {
    fifo[fifoWriteIndex++] = byte;
    if (fifoWriteIndex >= FIFO_SIZE)
        fifoWriteIndex = 0;
    fifoDataCount++;
}

uint8_t getHexByteFromFifo() {
    uint8_t c = hexCharToInt((char)fifoGet());
    uint8_t out = c << 4;
    c = hexCharToInt((char)fifoGet());
    out |= c;
    return out;
}

void uUSBLIB_DataReceivedHandler(uint16_t *Data, uint16_t Length) {
    uint8_t * strPtr = (uint8_t*)&Data[0];
    for (int n=0;n<Length;n++)
        fifoPut(*strPtr++);
}


